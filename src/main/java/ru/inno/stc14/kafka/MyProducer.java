package ru.inno.stc14.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class MyProducer {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.setProperty(ProducerConfig.CLIENT_ID_CONFIG, "MyProducer");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        try (KafkaProducer<Long, String> producer =
                     new KafkaProducer<>(properties)) {

            long time = System.currentTimeMillis();
            for (long i = time; i < time + 10; i++) {
                ProducerRecord<Long, String> record =
                        new ProducerRecord<>("testTopic", i,
                                "Hello, World! " + i);

                try {
                    RecordMetadata metadata = producer.send(record).get();
                    System.out.printf("key=%s, value=%s, meta(partition=%d, offset=%d)%n",
                            record.key(), record.value(),
                            metadata.partition(),
                            metadata.offset());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
